package singlelist.edittext;



import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import singleList.AbstractSingleLinkedListImpl;
import singleList.SingleLinkedListImpl;

public class SingleLinkedListTest {
	private AbstractSingleLinkedListImpl<Integer> lista;

	@Before
	public void setUp() throws Exception {
		lista = new SingleLinkedListImpl<Integer>();
	}

	@Test
	public void sizeListEmpty() {
		assertEquals("En una lista vacia el tamaño debe ser 0", lista.size(), 0);

	}

}
